# polygon
A basic *polygon container* module for the [Monkey programming language](https://github.com/blitz-research/monkey), meant to simplify handling raw 2D polygon-data.
